INSERT INTO Persons
	(PersonID, FirstName, LastName)
	VALUES
	(1, 'Bat', 'Man'),
	(2, 'Spider', 'Man'),
	(3, 'Iron', 'Man'),
	(4, 'Bruce', 'Lee'),
	(5, 'Jackie', 'Chan'),
	(6, 'Copy', 'Cat');
INSERT INTO Addresses
	(AddressID, Number, Street, City)
	VALUES
	(1, 5, 'Wayne Manor', 'Gotham'),
	(2, 8, 'Main Street', 'New York'),
	(3, 999, 'Stark Tower', 'New York'),
	(4, 55, 'Some Street', 'Hong Kong'),
	(5, 123, 'For The LOLZ', 'Beijing'),
	(6, 999, 'Stark Tower', 'New York');
INSERT INTO Food
	(FoodID, FoodName)
	VALUES
	(1, 'Fruit'),
	(2, 'Fly Soup'),
	(3, 'Scrap Metal'),
	(4, 'Noodles'),
	(5, 'Sushi'),
	(6, 'Sushi');