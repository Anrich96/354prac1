﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Controllers
{
    public class Address
    {
        public int AddressID { get; set; }
        public int AddressNumber { get; set; }
        public string Street { get; set; }
        public string City { get; set; }

    }
}