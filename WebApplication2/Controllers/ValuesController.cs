﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Dynamic;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;

namespace WebApplication2.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        //Conenction String: Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True

        ////////////////////////////////////////////////////////////////////////////START OF PERSON TABLE QUERIES
        [Route("api/Persons/getAllPersons")]
        [HttpPost]
        public List<Person> getAllPersons()
        {
            //PersonContext db = new PersonContext();
            //db.Configuration.ProxyCreationEnabled = false;
            List<Person> myList = new List<Person>();
            List<Address> addressList = new List<Address>();
            List<Food> foodList = new List<Food>();
            string queryString = "SELECT * FROM dbo.Persons;";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Person newPerson = new Person();
                    newPerson.PersonID = reader.GetInt32(0);
                    newPerson.FirstName = reader.GetString(1);
                    newPerson.LastName = reader.GetString(2);
                    myList.Add(newPerson);
                }
                reader.Close();
                foreach (Person per in myList)
                {
                    string queryString2 = "SELECT * FROM dbo.Addresses WHERE AddressID=" + per.PersonID + ";";
                    SqlCommand command2 = new SqlCommand(queryString2, conn);
                    SqlDataReader reader2 = command2.ExecuteReader();
                    if (reader2.HasRows)
                    {
                        while (reader2.Read())
                        {
                            Address newAddress = new Address();
                            newAddress.AddressID = reader2.GetInt32(0);
                            newAddress.AddressNumber = reader2.GetInt32(1);
                            newAddress.Street = reader2.GetString(2);
                            newAddress.City = reader2.GetString(3);
                            per.PersonAddress = newAddress;
                        }
                    }
                    reader2.Close();
                    string queryString3 = "SELECT * FROM dbo.Food WHERE FoodID=" + per.PersonID + ";";
                    SqlCommand command3 = new SqlCommand(queryString3, conn);
                    SqlDataReader reader3 = command3.ExecuteReader();
                    if (reader3.HasRows)
                    {
                        while (reader3.Read())
                        {
                            Food newFood = new Food();
                            newFood.FoodID = reader3.GetInt32(0);
                            newFood.FoodName = reader3.GetString(1);
                            per.PersonFood = newFood;
                        }
                    }
                    reader3.Close();
                }
                reader.Close();
            }
            else
                myList = null;
            return myList;
        }
        [HttpGet]
        [System.Web.Http.Route("api/Persons/Get")]

        [Route("api/Persons/getPersonByName")]
        [HttpPost]
        public List<basicPerson> getPersonByName(string name)
        {
            List<basicPerson> myList = new List<basicPerson>();
            string queryString = "SELECT * FROM dbo.Persons WHERE FirstName='"+name+"';";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    basicPerson newPerson = new basicPerson();
                    newPerson.PersonID = reader.GetInt32(0);
                    newPerson.FirstName = reader.GetString(1);
                    newPerson.LastName = reader.GetString(2);
                    myList.Add(newPerson);
                }
                reader.Close();
            }
            else
                myList = null;
            conn.Close();
            return myList;
        }
        
        [Route("api/Persons/getPersonBySurname")]
        [HttpPost]
        public List<basicPerson> getPersonByLastName(string lastname)
        {
            List<basicPerson> myList = new List<basicPerson>();
            string queryString = "SELECT * FROM dbo.Persons WHERE LastName='" + lastname + "';";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    basicPerson newPerson = new basicPerson();
                    newPerson.PersonID = reader.GetInt32(0);
                    newPerson.FirstName = reader.GetString(1);
                    newPerson.LastName = reader.GetString(2);
                    myList.Add(newPerson);
                }
                reader.Close();
            }
            else
                myList = null;
            conn.Close();
            return myList;
        }

        [Route("api/Persons/getPersonByID")]
        [HttpGet]
        public basicPerson getPersonByID(int id)
        {
            basicPerson newPerson = new basicPerson();
            string queryString = "SELECT * FROM dbo.Persons WHERE PersonID ='" + id + "';";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    newPerson.PersonID = reader.GetInt32(0);
                    newPerson.FirstName = reader.GetString(1);
                    newPerson.LastName = reader.GetString(2);
                    break;
                }
                reader.Close();
            }
            else
                newPerson = null;
            conn.Close();
            return newPerson;
        }

        [Route("api/Persons/getAllBasicPersons")]
        [HttpPost]
        public List<basicPerson> getAllBasicPersons()
        {
            List<basicPerson> myList = new List<basicPerson>();
            string queryString = "SELECT * FROM dbo.Persons;";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    basicPerson newPerson = new basicPerson();
                    newPerson.PersonID = reader.GetInt32(0);
                    newPerson.FirstName = reader.GetString(1);
                    newPerson.LastName = reader.GetString(2);
                    myList.Add(newPerson);
                }
                reader.Close();
            }
            else
                myList = null;
            conn.Close();
            return myList;
        }

        ////////////////////////////////////////////////////////////////////////////END OF PERSON TABLE QUERIES
        ////////////////////////////////////////////////////////////////////////////START OF ADDRESS TABLE QUERIES

        [Route("api/Persons/getAddressByStreetname")]
        [HttpPost]
        public List<Address> getAddressByStreetname(string street)
        {
            List<Address> myList = new List<Address>();
            string queryString = "SELECT * FROM dbo.Addresses WHERE Street LIKE '%" + street + "%';";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Address newPerson = new Address();
                    newPerson.AddressID = reader.GetInt32(0);
                    newPerson.AddressNumber = reader.GetInt32(1);
                    newPerson.Street = reader.GetString(2);
                    newPerson.City = reader.GetString(3);
                    myList.Add(newPerson);
                }
                reader.Close();
            }
            conn.Close();
            return myList;
        }

        [Route("api/Persons/getAddressByID")]
        [HttpGet]
        public Address getAddressByID(int id)
        {
            string queryString = "SELECT * FROM dbo.Addresses WHERE AddressID = " + id + ";";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            Address newPerson = new Address();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    newPerson.AddressID = reader.GetInt32(0);
                    newPerson.AddressNumber = reader.GetInt32(1);
                    newPerson.Street = reader.GetString(2);
                    newPerson.City = reader.GetString(3);
                    break;
                }
                reader.Close();
            }
            conn.Close();
            return newPerson;
        }
        [Route("api/Persons/getAllAddresses")]
        [HttpPost]
        public List<Address> getAllAddresses()
        {
            List<Address> myList = new List<Address>();
            string queryString = "SELECT * FROM dbo.Addresses;";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Address newPerson = new Address();
                    newPerson.AddressID = reader.GetInt32(0);
                    newPerson.AddressNumber = reader.GetInt32(1);
                    newPerson.Street = reader.GetString(2);
                    newPerson.City = reader.GetString(3);
                    myList.Add(newPerson);
                }
                reader.Close();
            }
            conn.Close();
            return myList;
        }


        [Route("api/Persons/getAddressByCity")]
        [HttpPost]
        public List<Address> getAddressByCity(string city)
        {
            List<Address> myList = new List<Address>();
            string queryString = "SELECT * FROM dbo.Addresses WHERE City LIKE '%" + city + "%';";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Address newPerson = new Address();
                    newPerson.AddressID = reader.GetInt32(0);
                    newPerson.AddressNumber = reader.GetInt32(1);
                    newPerson.Street = reader.GetString(2);
                    newPerson.City = reader.GetString(3);
                    myList.Add(newPerson);
                }
                reader.Close();
            }
            conn.Close();
            return myList;
        }


        [Route("api/Persons/getAddressByNumber")]
        [HttpGet]
        public List<Address> getAddressByNumber(int number)
        {
            string queryString = "SELECT * FROM dbo.Addresses WHERE Number = " + number + ";";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            List<Address> myList = new List<Address>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Address newPerson = new Address();
                    newPerson.AddressID = reader.GetInt32(0);
                    newPerson.AddressNumber = reader.GetInt32(1);
                    newPerson.Street = reader.GetString(2);
                    newPerson.City = reader.GetString(3);
                    myList.Add(newPerson);
                }
                reader.Close();
            }
            conn.Close();
            return myList;
        }
        ////////////////////////////////////////////////////////////////////////////END OF ADDRESS TABLE QUERIES
        ////////////////////////////////////////////////////////////////////////////START OF FOOD TABLE QUERIES

        [Route("api/Persons/getFoodByName")]
        [HttpPost]
        public List<Food> getFoodByName(string name)
        {
            List<Food> myList = new List<Food>();
            string queryString = "SELECT * FROM dbo.Food WHERE FoodName LIKE '%" + name + "%';";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Food newFood = new Food();
                    newFood.FoodID = reader.GetInt32(0);
                    newFood.FoodName = reader.GetString(1);
                    myList.Add(newFood);
                }
                reader.Close();
            }
            conn.Close();
            return myList;
        }

        [Route("api/Persons/getAllFood")]
        [HttpPost]
        public List<Food> getAllFood()
        {
            List<Food> myList = new List<Food>();
            string queryString = "SELECT * FROM dbo.Food;";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Food newFood = new Food();
                    newFood.FoodID = reader.GetInt32(0);
                    newFood.FoodName = reader.GetString(1);
                    myList.Add(newFood);
                }
                reader.Close();
            }
            conn.Close();
            return myList;
        }

        [Route("api/Persons/getFoodByID")]
        [HttpGet]
        public Food getFoodByID(int id)
        {
            Food newPerson = new Food();
            string queryString = "SELECT * FROM dbo.Food WHERE FoodID ='" + id + "';";
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            conn.Open();
            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    newPerson.FoodID = reader.GetInt32(0);
                    newPerson.FoodName = reader.GetString(1);
                    break;
                }
                reader.Close();
            }
            conn.Close();
            return newPerson;
        }

        ////////////////////////////////////////////////////////////////////////////END OF FOOD TABLE QUERIES
        ////////////////////////////////////////////////////////////////////////////INSERT Functions
        [Route("api/Persons/addPerson")]
        [HttpPost]
        public List<Person> addPerson([FromBody] Person person)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            string query = "INSERT INTO dbo.Persons (PersonID,FirstName,LastName) VALUES ("+ person.PersonID+", '"+ person.FirstName+"', '"+person.LastName+"');";
            string query2 = "INSERT INTO dbo.Addresses (AddressID, Number, Street, City) VALUES ("+ person.PersonAddress.AddressID+", "+ person.PersonAddress.AddressNumber+", '"+ person.PersonAddress.Street+"', '"+ person.PersonAddress.City+"');";
            string query3 = "INSERT INTO dbo.Food (FoodID, FoodName) VALUES ("+ person.PersonFood.FoodID+", '"+ person.PersonFood.FoodName+"');";
            SqlCommand command1 = new SqlCommand(query, conn);
            SqlCommand command2 = new SqlCommand(query2, conn);
            SqlCommand command3 = new SqlCommand(query3, conn);
            conn.Open();
            /*command1.Parameters.AddWithValue("@id", person.PersonID);
            command1.Parameters.AddWithValue("@first", person.FirstName);
            command1.Parameters.AddWithValue("@password", person.LastName);*/
            command1.ExecuteNonQuery();
            /*string test = query;
            Debug.WriteLine(test);
            test = query2;
            Debug.WriteLine(test);
            test = query3;
            Debug.WriteLine(test);*/

            /*command2.Parameters.AddWithValue("@id", person.PersonAddress.AddressID);
            command2.Parameters.AddWithValue("@number", person.PersonAddress.AddressNumber);
            command2.Parameters.AddWithValue("@street", person.PersonAddress.Street);
            command2.Parameters.AddWithValue("@city", person.PersonAddress.City);*/
            command2.ExecuteNonQuery();

            /*command3.Parameters.AddWithValue("@id", person.PersonFood.FoodID);
            command3.Parameters.AddWithValue("@food", person.PersonFood.FoodName);*/
            command3.ExecuteNonQuery();

            conn.Close();
            return getAllPersons();
        }

        [Route("api/Persons/addBasicPerson")]
        [HttpPost]
        public List<basicPerson> addBasicPerson([FromBody] basicPerson person)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            string query = "INSERT INTO dbo.Persons (PersonID,FirstName,LastName) VALUES (" + person.PersonID + ", '" + person.FirstName + "', '" + person.LastName + "');";
            SqlCommand command = new SqlCommand(query, conn);
            conn.Open();
            command.ExecuteNonQuery();

            conn.Close();
            return getAllBasicPersons();
        }


        [Route("api/Persons/addAddress")]
        [HttpPost]
        public List<Address> addAddress([FromBody] Address add)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            string query = "INSERT INTO dbo.Addresses (AddressID, Number, Street, City) VALUES (" + add.AddressID + ", " + add.AddressNumber + ", '" + add.Street + "', '" + add.City + "');";
            basicPerson test = new basicPerson();
            test = getPersonByID(add.AddressID);
            if (test == null)
            {
                return null;
            }
            else
            {
                SqlCommand command = new SqlCommand(query, conn);
                conn.Open();
                command.ExecuteNonQuery();
                return getAllAddresses();
            }
        }

        [Route("api/Persons/addFood")]
        [HttpPost]
        public List<Food> addFood([FromBody] Food add)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            string query = "INSERT INTO dbo.Food (FoodID, FoodName) VALUES (" + add.FoodID + ", '" + add.FoodName +"');";
            basicPerson test = new basicPerson();
            test = getPersonByID(add.FoodID);
            if (test == null)
            {
                return null;
            }
            else
            {
                SqlCommand command = new SqlCommand(query, conn);
                conn.Open();
                command.ExecuteNonQuery();
                return getAllFood();
            }
        }


        ////////////////////////////////////////////////////////////////////////////UPDATE Functions
        [Route("api/Persons/updatePerson")]
        [HttpPost]
        public List<Person> updatePersonByName([FromBody] Person person)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            string test = "SELECT * FROM dbo.Persons WHERE FirstName = '" + person.FirstName + "';";
            conn.Open();
            SqlCommand testCommand = new SqlCommand(test, conn);
            SqlDataReader reader = testCommand.ExecuteReader();
            if (reader.HasRows)
            {
                string query = "UPDATE dbo.Persons SET LastName='" + person.LastName + "' WHERE FirstName='"+ person.FirstName + "';";
                string query2 = "UPDATE dbo.Addresses SET Number="+person.PersonAddress.AddressNumber + ", Street='" + person.PersonAddress.Street + "', City='" + person.PersonAddress.City + "' WHERE AddressID="+ person.PersonID+";";
                string query3 = "UPDATE dbo.Food SET FoodName='"+person.PersonFood.FoodName + "' WHERE FoodID="+ person.PersonID + ";";
                Debug.WriteLine(query);
                Debug.WriteLine(query2);
                Debug.WriteLine(query3);
                SqlCommand command1 = new SqlCommand(query, conn);
                SqlCommand command2 = new SqlCommand(query2, conn);
                SqlCommand command3 = new SqlCommand(query3, conn);
                reader.Close();
                command1.ExecuteNonQuery();
                command2.ExecuteNonQuery();
                command3.ExecuteNonQuery();

                conn.Close();
                return getAllPersons();
            }
            else
            {
                conn.Close();
                return null;
            }
        }

        [Route("api/Persons/updatePersonByID")]
        [HttpPost]
        public List<Person> updatePersonByID([FromBody] Person person)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            string test = "SELECT * FROM dbo.Persons WHERE  PersonID = '" + person.PersonID + "';";
            conn.Open();
            SqlCommand testCommand = new SqlCommand(test, conn);
            SqlDataReader reader = testCommand.ExecuteReader();
            if (reader.HasRows)
            {
                string query = "UPDATE dbo.Persons SET FirstName= '"+person.FirstName+"', LastName='" + person.LastName + "' WHERE PersonID='" + person.PersonID + "';";
                string query2 = "UPDATE dbo.Addresses SET Number=" + person.PersonAddress.AddressNumber + ", Street='" + person.PersonAddress.Street + "', City='" + person.PersonAddress.City + "' WHERE AddressID=" + person.PersonID + ";";
                string query3 = "UPDATE dbo.Food SET FoodName='" + person.PersonFood.FoodName + "' WHERE FoodID=" + person.PersonID + ";";
                SqlCommand command1 = new SqlCommand(query, conn);
                SqlCommand command2 = new SqlCommand(query2, conn);
                SqlCommand command3 = new SqlCommand(query3, conn);
                reader.Close();
                command1.ExecuteNonQuery();
                command2.ExecuteNonQuery();
                command3.ExecuteNonQuery();

                conn.Close();
                return getAllPersons();
            }
            else
            {
                conn.Close();
                return null;
            }
        }

        [Route("api/Persons/updateAddressByID")]
        [HttpPost]
        public List<Person> updateAddressByID([FromBody] Address add)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            string test = "SELECT * FROM dbo.Addresses WHERE  AddressID = '" + add.AddressID + "';";
            conn.Open();
            SqlCommand testCommand = new SqlCommand(test, conn);
            SqlDataReader reader = testCommand.ExecuteReader();
            if (reader.HasRows)
            {
                string query = "UPDATE dbo.Addresses SET Number=" + add.AddressNumber + ", Street='" + add.Street + "', City='" + add.City + "' WHERE AddressID=" + add.AddressID + ";";
                SqlCommand command1 = new SqlCommand(query, conn);
                reader.Close();
                command1.ExecuteNonQuery();

                conn.Close();
                return getAllPersons();
            }
            else
            {
                conn.Close();
                return null;
            }
        }

        [Route("api/Persons/updateFoodByID")]
        [HttpPost]
        public List<Person> updateFoodByID([FromBody] Food add)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            string test = "SELECT * FROM dbo.Food WHERE  FoodID = '" + add.FoodID + "';";
            conn.Open();
            SqlCommand testCommand = new SqlCommand(test, conn);
            SqlDataReader reader = testCommand.ExecuteReader();
            if (reader.HasRows)
            {
                string query = "UPDATE dbo.Food SET FoodName='" + add.FoodName + "' WHERE FoodID=" + add.FoodID + ";";
                SqlCommand command1 = new SqlCommand(query, conn);
                reader.Close();
                command1.ExecuteNonQuery();

                conn.Close();
                return getAllPersons();
            }
            else
            {
                conn.Close();
                return null;
            }
        }
        ////////////////////////////////////////////////////////////////////////////DELETE Functions

        [Route("api/Persons/deletePersonByID")]
        [HttpPost]
        public List<Person> deletePersonByID(int id)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-T1VUT2C\\SQLEXPRESS;Initial Catalog=PersonDB;Integrated Security=True");
            string test = "SELECT * FROM dbo.Persons WHERE PersonID = '" + id + "';";
            conn.Open();
            SqlCommand testCommand = new SqlCommand(test, conn);
            SqlDataReader reader = testCommand.ExecuteReader();
            if (reader.HasRows)
            {
                string query = "DELETE FROM dbo.Persons WHERE PersonID='" + id + "';";
                string query2 = "DELETE FROM dbo.Addresses WHERE AddressID=" + id + ";";
                string query3 = "DELETE FROM dbo.Food WHERE FoodID=" + id + ";";
                SqlCommand command2 = new SqlCommand(query2, conn);
                SqlCommand command3 = new SqlCommand(query3, conn);
                SqlCommand command1 = new SqlCommand(query, conn);
                reader.Close();
                command2.ExecuteNonQuery();
                command3.ExecuteNonQuery();
                command1.ExecuteNonQuery();

                conn.Close();
                return getAllPersons();
            }
            else
            {
                conn.Close();
                return null;
            }
        }

        ////////////////////////////////////////////////////////////////////////////Other
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
        private List<dynamic> personReturnList(List<Person> persons)
        {
            List<dynamic> people = new List<dynamic>();
            foreach(Person person in persons)
            {
                dynamic dynamicPerson = new ExpandoObject();
                dynamicPerson.ID = person.PersonID;
                dynamicPerson.firstName = person.FirstName;
                dynamicPerson.lastName = person.LastName;
                dynamicPerson.address = person.PersonAddress;
                dynamicPerson.food = person.PersonFood;
                people.Add(dynamicPerson);
            }
            return people;
        }
        private Person ReadSingleRow(IDataRecord record)
        {
            Console.WriteLine(String.Format("{0}, {1}", record[0], record[1]));
            return null;
        }
    }
}
