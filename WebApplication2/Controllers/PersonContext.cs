﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication2.Controllers
{
    public class PersonContext : DbContext
    {
        public PersonContext() : base()
        {

        }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Food> Food { get; set; }
    }
}