CREATE DATABASE PersonDB;
CREATE TABLE Persons
(
	PersonID int NOT NULL Primary KEY,
	FirstName varchar(255),
	LastName varchar(255),
);

CREATE TABLE Addresses
(
	AddressID int NOT NULL PRIMARY KEY,
	Number int,
	Street varchar(255),
	City varchar(255),
	FOREIGN KEY (AddressID) REFERENCES Persons (PersonID)
);
CREATE TABLE Food
(
	FoodID int NOT NULL Primary Key,
	FoodName varchar(255),
	FOREIGN KEY (FoodID) REFERENCES Persons (PersonID)
);